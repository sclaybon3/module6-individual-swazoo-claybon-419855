var http = require('http'),
    fs = require('fs');

http.createServer(function(req, res) {
  if(req.url == "/" || req.url == "/index.html") {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(fs.readFileSync('index.html')); 
  } else if(req.url == "/css/bootstrap.min.css") {
    res.writeHead(200, {'Content-Type': 'text/css'});
    res.end(fs.readFileSync('css/bootstrap.min.css')); 
  }else if(req.url == "/css/simple-sidebar.css") {
    res.writeHead(200, {'Content-Type': 'text/css'});
    res.end(fs.readFileSync('css/simple-sidebar.css')); 
  }else if(req.url == "/js/bootstrap.min.js") {
    res.writeHead(200, {'Content-Type': 'application/javascript'});
    res.end(fs.readFileSync('js/bootstrap.min.js')); 
  }  else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end("Page Could Not Be Found"); 
  }
}).listen(3456);