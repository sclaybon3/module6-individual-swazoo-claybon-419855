// Require the packages we will use:


var http = require('http'),
    socketio = require("socket.io"),
    fs = require('fs');

var app = http.createServer(function(req, res) {
  if(req.url == "/" || req.url == "/index.html") {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(fs.readFileSync('index.html')); 
  } else if(req.url == "/css/bootstrap.min.css") {
    res.writeHead(200, {'Content-Type': 'text/css'});
    res.end(fs.readFileSync('css/bootstrap.min.css')); 
  }else if(req.url == "/css/simple-sidebar.css") {
    res.writeHead(200, {'Content-Type': 'text/css'});
    res.end(fs.readFileSync('css/simple-sidebar.css')); 
  }else if(req.url == "/js/bootstrap.min.js") {
    res.writeHead(200, {'Content-Type': 'application/javascript'});
    res.end(fs.readFileSync('js/bootstrap.min.js')); 
  }else if(req.url == "/lib/sweet-alert.js") {
    res.writeHead(200, {'Content-Type': 'application/javascript'});
    res.end(fs.readFileSync('lib/sweet-alert.js')); 
  }  else if(req.url == "/lib/sweet-alert.css") {
    res.writeHead(200, {'Content-Type': 'text/css'});
    res.end(fs.readFileSync('lib/sweet-alert.css')); 
  }else if(req.url == "/fiftyshades.mp3") {
        var seMp2 = fs.readFileSync('./fiftyshades.mp3');
        res.writeHead(200, {'Content-Type':'audio/mp3' });
        res.end(seMp2, 'binary');
  } else if(req.url == "/lock.png") {
        var seMp2 = fs.readFileSync('./lock.png');
        res.writeHead(200, {'Content-Type':'image/png' });
        res.end(seMp2, 'binary');
  }    else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end("Page Could Not Be Found"); 
  }
})

app.listen(3456);


/*
var http = require("http"),
socketio = require("socket.io"),
fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, res){
	// This callback runs when a new connection is made to our HTTP server.
 
 
 if(req.url == "/" || req.url == "/index.html") {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(fs.readFileSync('index.html')); 
  }else if(req.url == "css/bootstrap.min.css") {
    res.writeHead(200, {'Content-Type': 'text/css'});
    res.end(fs.readFileSync('css/bootstrap.min.css')); 
  }else if(req.url == "css/simple-sidebar.css") {
    res.writeHead(200, {'Content-Type': 'text/css'});
    res.end(fs.readFileSync('css/simple-sidebar.css')); 
  }else if(req.url == "js/bootstrap.min.css") {
    res.writeHead(200, {'Content-Type': 'application/javascript'});
    res.end(fs.readFileSync('js/bootstrap.min.js')); 
  }else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end("Page Could Not Be Found"); 
  }
 /*
 
	fs.readFile("index.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
       
});
app.listen(3456);

*/





////////////////Variables I will use are below///////////////


var users = [];
var saved_messages = [];
var people = [];
var a = ['value1', 'value2', 'value3'];


var chatRooms = [{'name': "lobby", 'users': users, 'creator': "administration"}];

/*var count = Object.keys(chatRooms[0]).length;
console.log(count);
console.log(chatRooms[0].length);*/

//if chatRooms has 4 elements in the array, that means it is password protected
//at this point, search for the password and only allow the person to join the room if the
//input password and JSON password are the same, otherwise alert "sorry that was not the correct password"

var id_list = []
//[{'name': "", 'id': ""}];
//id_list.push[{name: username, id: socket_id}];
var ban = [{'name': "", 'room': "", 'id': ""}];
//ban.push[{name: username, room: room_name, id: socket_id}];



////////////////Variables I will use are above//////////////


 
////////////functions I will use//////////////////

function isInArray(value, array) {
  return array.indexOf(value) > -1;
};


function stackoverflow_removeArrayItem(array, itemToRemove) {
    // Count of removed items
    var removeCounter = 0;

    // Iterate every array item
    for (var index = 0; index < array.length; index++) {
        // If current array item equals itemToRemove then
        if (array[index] === itemToRemove) {
            // Remove array item at current index
            array.splice(index, 1);

            // Increment count of removed items
            removeCounter++;

            // Decrement index to iterate current position 
            // one more time, because we just removed item 
            // that occupies it, and next item took it place
            index--;
        }
    }

    // Return count of removed items
    return removeCounter;
}

////////////functions are above///////////////////

// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
 
	socket.on('message_to_server', function(message, username) {
		// This callback runs when the server receives a new message from the client.
 /*
		console.log("message: "+data["message"]); // log it to the Node.JS output
		io.sockets.emit("message_to_client",{message:data["message"], name:data["user"] }) // broadcast the message to other users
	
               */
        console.log("wait for it");
        console.log(message);
        
    
        //if the username is in any of the rooms,
        //only broadcast to the people in that room
        
        count = 0;
        for (i=0; i<chatRooms.length; i++) {
              if (chatRooms[i].users.indexOf(username) > -1){
              console.log(chatRooms[i].name);
              console.log("wakeup!!!");
              console.log("logged user" + username);
              count = count+1;
              io.sockets.in(chatRooms[i].name).emit("message_to_client", message , username);
              }}
              
        /*
        for (i=0; i<chatRooms.length; i++) {
              if (!(chatRooms[i].users.indexOf(username) > -1) && i==(chatRooms.length-1)){
              //count = count+1;
              io.sockets.emit("message_to_client", message , username);
              }
              }
        */      
        console.log("count=" + count);
        
        });
        
      
      
        socket.on('switchover_kai', function(username, roomname) {
            
            socket.leave(roomname);
            console.log(username + "has left" + roomname);
        
        });
        
        socket.on('kick_from_room_kai', function(username, roomname) {
            
            socket.leave(roomname);
            console.log(username + "has left" + roomname);
        
        });
      
      
        socket.on('adduser', function(name) {
		//sign into the lobby with a username and store it in an array.
                
                if (people.indexOf(name) > -1 || name==null || name=='null'){
                    
                    console.log(name + "is not available");
                    io.sockets.emit("unavailable");
                    
                }else{
                
                people.push(name);
                console.log("people:");
                console.log(people[0]);
                console.log(people[1]);
                console.log(people[2]);
                console.log(people[3]);
                
                //add the user
                
                socket.join("lobby");
                chatRooms[0].users.push(name);
                
                a = JSON.stringify(people);
                
                console.log(socket.id);
                
                id_list.push({'name': name, 'id': socket.id});
                
                console.log(id_list);
                
                //io.sockets.emit("people_message", {person:people[0]});
	        io.sockets.emit("people_message", a );
                
                }
	});
        
        
        socket.on('Whisper', function(whispername, message, username) {
            
            console.log(whispername + " " + message + " " + username);
            
            for (i=0; i<chatRooms.length; i++) {
                
                console.log("1");
                if (chatRooms[i].users.indexOf(whispername) > -1){
                    
                    console.log("2");
                    for (m=0; m<id_list.length; m++) {
                        
                        console.log("3");
                        if (id_list[m].name == whispername){
                            
                            console.log("4");
                            io.to(id_list[m].id).emit("whisper_to", username, message);
                        }
                    }
                }
            }
                            
            
        });
        
        
        socket.on('Kick_Out', function(username, creatorname){
         
          for (i=0; i<chatRooms.length; i++) {
                console.log("1");
                if (chatRooms[i].creator.indexOf(creatorname) > -1){
                    console.log("2");
                    console.log(id_list.length);
                    for (m=0; m<id_list.length; m++) {
                        console.log("3");
                        console.log("2.5");
                        console.log(id_list);
                        if (id_list[m].name == username){
                            console.log("username = " + username);
                            console.log(id_list[m].id);
                            
                            io.to(id_list[m].id).emit("message_to_client", "You have been kicked out", username);
                            
                            io.to(id_list[m].id).emit("kick_from_room", username, chatRooms[i].name);
                            //socket.join(id_list[m].id);
                            
                            
                            io.sockets.in(chatRooms[i].name).emit("message_to_client", "has been kicked" , username);
                            itemsRemoved = stackoverflow_removeArrayItem(chatRooms[i].users, username);
                            chatRooms[0].users.push(username);
                            console.log(chatRooms);
                            
                        //if the name is not in there and it is the last row, alert to the creator, You must type a valid name    
                        }
                    }
                }
          }
        });
        
        socket.on('Ban', function(username, creatorname, roomname){
         
          for (i=0; i<chatRooms.length; i++) {
                console.log("1");
                if (chatRooms[i].creator.indexOf(creatorname) > -1){
                    console.log("2");
                    console.log(id_list.length);
                    for (m=0; m<id_list.length; m++) {
                        console.log("3");
                        console.log("2.5");
                        console.log(id_list);
                        if (id_list[m].name == username){
                            console.log("username = " + username);
                            console.log(id_list[m].id);
                            
                            io.to(id_list[m].id).emit("message_to_client", "You have been banned", username);
                            
                            io.to(id_list[m].id).emit("switchover", username, roomname);
                            //socket.join(id_list[m].id);
                            
                            
                            io.sockets.in(chatRooms[i].name).emit("message_to_client", "has been banned" , username);
                            itemsRemoved = stackoverflow_removeArrayItem(chatRooms[i].users, username);
                            chatRooms[0].users.push(username);
                            console.log(chatRooms);
                            //io.sockets.in(chatRooms[1].name).emit("room_joining", "lobby", username, c);
                            //io.to(id_list[m].id).leave(chatRooms[i]);
                            ban.push({'name': username, 'room': roomname});
                            console.log(ban);
                        //if the name is not in there and it is the last row, alert to the creator, You must type a valid name    
                        }
                    }
                }
          }
        });
        
        
        
        socket.on('Create_Room', function(room_name,username){
         
             a = [];
             var change = 1;
            
             
             //iterate through the room names and see if one has already been taken.
             //if so, emit back to the creator and tell them
             for (i=0; i<chatRooms.length; i++) {
             if(chatRooms[i].name == room_name){
                
                for (m=0; m<id_list.length; m++) {
                        console.log("going through id_list");
                        if (id_list[m].name == username){
                console.log("list completed");
                console.log(id_list[m].id);
                console.log(socket.id);
                change = 2;
                //io.to(id_list[m].id).emit("chatroom_taken", "This chatroom was already taken");
                io.sockets.emit("room_not_available", "This chatroom was already taken");
                            
                        }
                }
             }}
             
             if (change==1) {
        
             chatRooms.push(
                 {'name': room_name, 'users': a, 'creator': username}
                );
             a = JSON.stringify(chatRooms);
             b = JSON.parse(a);
             console.log(b);
	     var count = Object.keys(chatRooms[0]).length;
	     console.log(count);
	     console.log("testing this");
             //console.log(chatRooms);
             //io.sockets.emit("room_not_available", "This chatroom was already taken");
             io.sockets.emit("room_creation", a );
             }
             
        });
        
        
        socket.on('create_private_kai', function(room_name, room_pass, username){
            
            //[{'name': "lobby", 'users': users, 'creator': "administration", 'pass': ""}];
         
             a = [];
             var change = 1;
             console.log("inside the cprivate loop");
             
             //iterate through the room names and see if one has already been taken.
             //if so, emit back to the creator and tell them
             for (i=0; i<chatRooms.length; i++) {
                
                console.log("1");
                
             if(chatRooms[i].name == room_name){
                
                console.log("2");
                
                for (m=0; m<id_list.length; m++) {
                        console.log("going through id_list");
                        if (id_list[m].name == username){
                console.log("list completed");
                console.log(id_list[m].id);
                console.log(socket.id);
                change = 2;
                //io.to(id_list[m].id).emit("chatroom_taken", "This chatroom was already taken");
                io.sockets.emit("room_not_available", "This chatroom was already taken");
                            
                        }
                }
             }}
             
             if (change==1) {
        
             chatRooms.push(
                 {'name': room_name, 'users': a, 'creator': username, 'pass': room_pass}
                );
             a = JSON.stringify(chatRooms);
             b = JSON.parse(a);
             console.log(b);
             //console.log(chatRooms);
             //io.sockets.emit("room_not_available", "This chatroom was already taken");
             io.sockets.emit("room_creation", a );
             }
             
        });
        
        
        socket.on('Room_Click', function(nothing){
            
            console.log("Room_Click_function");
            //send the chatRooms array back out
            
            a = JSON.stringify(chatRooms);
            console.log(a)
	    b = JSON.parse(a);
	    console.log("Length is" + b[0].length);
	    console.log("Length is" + a[0].length);
	    var count1 = Object.keys(b[0]).length;
	    console.log(count1);
	    console.log(b);
            
            //send all room names here where they will be processed and sent to the a script
            //that will print it out on an href for single click room joining for anybody
            //have an if statement that returns a sweet alert telling them if the room has a password and to input it
            io.sockets.emit("Room_Open", a);
         
         
            
        });
        
        
        
        
         socket.on('Join_Room', function(room_name,username){
         
              
                console.log("sooo close");
            
                
              for (i=0; i<chatRooms.length; i++) {
            
//////////////////////////////if room_name == lobby//////////////////////////
               if (room_name == "lobby"){
                
                console.log("we are in the lobby");
                
               //leave the chatroom and go back to the lobby
                for(k=0; k<chatRooms[i].users.length; k++){
                  
                  console.log(chatRooms[i].users.length);
                  console.log(room_name);
                    
                
                    
                  console.log("passed the room name");
                  console.log(username);
                    
                  console.log(chatRooms[i].users[k]);  
                    
                  if (chatRooms[i].users.indexOf(username) > -1){
                    
                    console.log("made it FTW!!!");
                    
                    //console.log(chatRooms[i-1].users[k-1]);
                    
                    console.log(chatRooms[i].name);
                    console.log(chatRooms[0].name);
                    
                    socket.leave(chatRooms[i].name);
                    socket.join(chatRooms[0].name);
                        
                    itemsRemoved = stackoverflow_removeArrayItem(chatRooms[i].users, username);
                    
                    chatRooms[0].users.push(username);
                    
                    c = "leave";
                    //io.sockets.emit("room_joining", room_name, username, c);
                    io.sockets.in(chatRooms[i].name).emit("room_joining", chatRooms[i].name , username, c);
                    //username has joined the lobby
                    io.sockets.in(chatRooms[0].name).emit("room_joining_kai", " has joined the lobby", username);
                    //break;
                }
                
               }
     //////////////// if the name is in the list of rooms with a check for ban///////////////          
                } else if ((chatRooms[i].name.indexOf(room_name) > -1 ) && (chatRooms[i].length > 3)) {
                    
                    console.log(chatRooms[i]);
                    
                    for (m=0; m<id_list.length; m++) {
                        console.log("going through id_list");
                        if (id_list[m].name == username){
                    io.to(id_list[m].id).emit("room_not_available", "This chatroom is password protected...");
                        }}
                    
                    
                }else if((chatRooms[i].name.indexOf(room_name) > -1 )){
                
                 //if the name given is one of the created rooms
                 console.log("inside the chatroom database");
                 
                // ban.push[{name: username, room: room_name, id: socket_id}];
                //if the name is not in the ban list for that specific roomname, then continue
                 
                 
                var banned = 1 //not banned ==1
                 
                for (q=0; q<ban.length; q++){
                    if (username==ban[q].name && room_name==ban[q].room) {
                        var banned = 2; // banned ==2
                    }
                }
                
                
                console.log("banned is " + banned);
                 
///////////////if not banned////////////////////////// 
                 if (banned==1) {
                    
                    
                    if(!(chatRooms[i].users.indexOf(username) > -1)){
                    
                 //////////////break point/////////////////   
                 
                 chatRooms[i].users.push(username);    
                //console.log(chatRooms[i].users[0]);
                //console.log(chatRooms[i]);
                
                socket.leave("lobby");
                socket.join(chatRooms[i].name);
                //remove the name from the users array
                
                
                itemsRemoved = stackoverflow_removeArrayItem(chatRooms[0].users, username);
    
                
                
                b = chatRooms[i].users[chatRooms[i].users.length-1];
                console.log(b);
                io.sockets.in(chatRooms[i].name).emit("room_joining", room_name, b);
                //io.sockets.emit("room_joining", room_name, b);
                
                //username has left the lobby
                io.sockets.in(chatRooms[0].name).emit("room_joining_kai", "has left the lobby", username);
                    
                console.log(chatRooms);
                    }else{
                        
                        for (m=0; m<id_list.length; m++) {
                        console.log("going through id_list");
                        if (id_list[m].name == username){

                         io.to(id_list[m].id).emit("chatroom_not_available", "You are already logged in...");
                            }
                        }
                    
                    }
                
                ////////////break point///////////////
                   
                 
                 }else{
                    
                    
                    for (m=0; m<id_list.length; m++) {
                        console.log("going through id_list");
                        if (id_list[m].name == username){
                    
                //io.sockets.emit("room_not_available", "You were banned from this room!");
                
                io.to(id_list[m].id).emit("room_not_available", "You were banned from this room!");
                
                        }}
                
                }
                
                
                
                }
  //////////////end of if name is in the user array for the room////////////////////              
           
              }   
         
             
             
             
        });
         
         
         
         socket.on('join_private_kai', function(room_name, room_pass, username){
                
             for (i=0; i<chatRooms.length; i++) {
            
             if (chatRooms[i].name.indexOf(room_name) > -1){
                
                 console.log("inside the chatroom database");
                 
                // ban.push[{name: username, room: room_name, id: socket_id}];
                //if the name is not in the ban list for that specific roomname, then continue
                 
                 
                var banned = 1 //not banned ==1
                 
                for (q=0; q<ban.length; q++){
                    if (username==ban[q].name && room_name==ban[q].room) {
                        var banned = 2; // banned ==2
                    }
                }
                
                
                console.log("banned is " + banned);
                 
///////////////if not banned////////////////////////// 
                 if (banned==1) {
                    
                    
                    if(!(chatRooms[i].users.indexOf(username) > -1) && (chatRooms[i].pass == room_pass)){
                    
                 //////////////break point/////////////////   
                 
                 chatRooms[i].users.push(username);    
                //console.log(chatRooms[i].users[0]);
                //console.log(chatRooms[i]);
                
                socket.leave("lobby");
                socket.join(chatRooms[i].name);
                //remove the name from the users array
                
                
                itemsRemoved = stackoverflow_removeArrayItem(chatRooms[0].users, username);
    
                
                
                b = chatRooms[i].users[chatRooms[i].users.length-1];
                console.log(b);
                io.sockets.in(chatRooms[i].name).emit("room_joining", room_name, b);
                //io.sockets.emit("room_joining", room_name, b);
                
                //username has left the lobby
                io.sockets.in(chatRooms[0].name).emit("room_joining_kai", "has left the lobby", username);
                    
                console.log(chatRooms);
                    }else{
                        
                        for (m=0; m<id_list.length; m++) {
                        console.log("going through id_list");
                        if (id_list[m].name == username){

                         io.to(id_list[m].id).emit("chatroom_not_available", "You are already logged in...");
                            }
                        }
                    
                    }
                
                ////////////break point///////////////
                   
                 
                 }else{
                    
                    
                    for (m=0; m<id_list.length; m++) {
                        console.log("going through id_list");
                        if (id_list[m].name == username){
                    
                //io.sockets.emit("room_not_available", "You were banned from this room!");
                
                io.to(id_list[m].id).emit("room_not_available", "You were banned from this room!");
                
                        }}
                
                }
                
                
                
                }
  //////////////end of if name is in the user array for the room////////////////////              
           
              }   
         
             
             
             
        });
         
         
         
         
});





